const path = require('path')
const SequelizeAuto = require('sequelize-auto')

exports.gen = async (config) => {
    let default_model_path = config['default_model_path'] || 'resource/model'
    let _db_names = Object.keys(config.db);

    let db_config = config.db;

    for (let i = 0; i < _db_names.length; i++) {
        let name = _db_names[i]

        if(db_config[name]['dialect'] === 'mongo') continue;

        db_config[name]['tables'] = []
        if (db_config[name]['tables'].length <= 0) delete db_config[name]['tables']
        db_config[name]['directory'] = path.join(process.cwd(), default_model_path, name)

        let auto = new SequelizeAuto(db_config[name].database, db_config[name].username, db_config[name].password, db_config[name])

        await auto.run()
    }
}
