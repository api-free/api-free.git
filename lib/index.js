const _ = require('lodash')
const database = require('./utils/database')
const parser = require('./parser')
const operate_parse = require('./parser/operate_parse')


exports.init = async (config) => {
    return parser.init(config)
}
exports.parse = async (params = {}) => {
    return parser.parseOperator(_.toPairs(params))
}

exports.M = database.M
exports.DB = database.DB

exports.dbUtil = {
    getRow:operate_parse.getRow,
    getList:operate_parse.getList,
    getAll:operate_parse.getAll,
    fillData:operate_parse.fillData,
    createOrUpdate:operate_parse.createOrUpdate,
}
