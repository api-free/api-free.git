module.exports = {
    $row: {                     // 查询一条结果
        $validate: {},          // 验证$where中的参数
        $model: '',             // 当前数据库模型
        $where: {},             // 查询条件
        $order: {},             // 排序
        $attribute: {},         // 需要返回的字段 默认 select *
        $include: [{            // 补充查询
            $model: '',         // 补充查询的模型
            $foreign: '',       // 当前数据库模型的外键
            $target: '',        // 补充查询的模型的外键
            $where: {},         // 查询条件
            $order: '',         // 排序
            $attribute: {},     // 需要返回的字段 默认 select *
            $as: '',            // 返回结果的名字默认为模型名称
            $split: ',',        // 分割符
            $relation: '',      // 查询方式 one,many,has,array
            $include: [{}]      // 子子补充查询
        }],
        $join: [{
            $model: '',
            $foreign: '',
            $target: '',
            $where: {},
            $attribute: {},
        }],
    },

    $list: {                    // 分页查询
        $validate: {},
        $model: '',
        $page: 1,
        $limit: 10,
        $where: {},
        $order: {},
        $attribute: {},
        $include: [{}],
        $join: [{}],
    },

    $all: {                     // 查询所有
        $validate: {},
        $model: '',
        $where: {},
        $order: {},
        $attribute: {},
        $include: [{}],
        $join: [{}],
    },

    $add: {                     // 新增数据
        $model: '',
        $params: {},            // 可以是数组支持批量操作, $id,$shortid,$now 变量处理
    },

    $update: {                  // 修改数据
        $validate: {},
        $model: '',
        $where: {},
        $params: {},
    },

    $delete: {                  // 删除数据
        $validate: {},
        $model: '',
        $where: {},
    },
}
