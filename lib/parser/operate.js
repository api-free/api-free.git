const parse = require('./operate_parse')
const helper = require('../utils/helper')
const _ = require('lodash')

module.exports = {
    $row: async (ctx = {}) => {
        let params = ctx.params || {}

        let { $validate, $model, $where, $order, $attribute, $include, $cache } = params
        let rule = {
            $validate: { type: 'object?', default: {} },
            $model: { type: 'string' },
            $where: { type: 'object' },
            $order: { type: 'arrayOrObject?', default: [] },
            $attribute: { type: 'arrayOrObject?', default: [] },
            $include: { type: 'arrayOrObject?', default: [] },
            $cache: { type: 'object?', default: {} },
        }

        helper.validate(rule, params)

        !_.isEmpty($validate) && helper.validate($validate || {}, $where || {})

        return { data: await parse.getRow($model, $where, $order, $include, $attribute), params }
    },

    $list: async (ctx = {}) => {
        let params = ctx.params || {}

        let { $validate, $model, $page, $limit, $where, $order, $attribute, $include, $cache } = params
        let rule = {
            $validate: { type: 'object?', default: {} },
            $model: { type: 'string' },
            $where: { type: 'object' },
            $page: { type: 'int?', convertType: 'int', default: 1 },
            $limit: { type: 'int?', convertType: 'int', default: 10 },
            $order: { type: 'arrayOrObject?', default: [] },
            $attribute: { type: 'arrayOrObject?', default: [] },
            $include: { type: 'arrayOrObject?', default: [] },
            $cache: { type: 'object?', default: {} },
        }

        helper.validate(rule, params)

        !_.isEmpty($validate) && helper.validate($validate || {}, $where || {})

        return { data: await parse.getList($model, $where, $page, $limit, $order, $include, $attribute), params }
    },

    $all: async (ctx = {}) => {
        let params = ctx.params || {}
        let { $validate, $model, $where, $order, $attribute, $include, $cache } = params
        let rule = {
            $validate: { type: 'object?', default: {} },
            $model: { type: 'string' },
            $where: { type: 'object' },
            $order: { type: 'arrayOrObject?', default: [] },
            $attribute: { type: 'arrayOrObject?', default: [] },
            $include: { type: 'arrayOrObject?', default: [] },
            $cache: { type: 'object?', default: {} },
        }

        helper.validate(rule, params)

        !_.isEmpty($validate) && helper.validate($validate || {}, $where || {})

        return { data: await parse.getAll($model, $where, $order, $include, $attribute), params }
    },

    $add: async (ctx = {}) => {
        let params = ctx.params || {}
        let { $model, $params } = params
        let rule = {
            $model: { type: 'string' },
            $params: { type: 'arrayOrObject?', default: [] },
        }

        helper.validate(rule, params)

        return { data: await parse.add($model, helper.parseParams($params)), params }
    },

    $update: async (ctx = {}) => {
        let params = ctx.params || {}
        let { $validate, $model, $where, $params } = params
        let rule = {
            $validate: { type: 'object?', default: {} },
            $model: { type: 'string' },
            $where: { type: 'object' },
            $params: { type: 'object' },
        }

        helper.validate(rule, params)

        !_.isEmpty($validate) && helper.validate($validate || {}, $where || {})

        return { data: await parse.update($model, $where, helper.parseParams($params)), params }
    },

    $delete: async (ctx = {}) => {
        let params = ctx.params || {}
        let { $validate, $model, $where } = params
        let rule = {
            $validate: { type: 'object?', default: {} },
            $model: { type: 'string' },
            $where: { type: 'object' },
        }

        helper.validate(rule, params)

        !_.isEmpty($validate) && helper.validate($validate || {}, $where || {})

        return { data: await parse.delete($model, $where), params }
    },
}
