api-free 是Node.js 实现的一套操作规范，可以将简单重复的增删改查，自动实现，降低接口开发成本，提高开发效率。

###  特点功能(less is more)
-  提供通用接口，大部分接口不用重复提供
- 支持mongodb, mysql, postgres, mariadb, seqlite3, sql server
- 支持多数据库连接
- 以npm包的方式提供，集成简单，可以在任何框架中集成（koa,express等等）
- 支持参数验证
- 支持通过before和after 函数，拦截或者再次处理结果
- 底层使用sequelize和mongoose 不重复造轮子。
- 支持自动生成数据库模型（mongodb 除外）
- 支持任意补全请求中的外联id，可以跨库，跨数据库类型（sql 和 mongo 相互）
- 支持mongodb 事务（mongodb 4.2 以上版本支持）
- 支持复用数据库模型，复杂的接口可以直接使用api-free导出的模型，无需重新配置数据库
